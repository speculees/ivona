'''
Created on Jan 10, 2014

@author: Ivona
'''
import os

def promeniKomponentu(komponenta, lkategorije):
    os.system("clear")   
    print "IZABRANA KOMPONENTA: "+komponenta.get_naziv()         
    print "  1)Cena: "+komponenta.get_cena()
    print "  2)Raspoloziva kolicina: "+komponenta.get_raspoloziva_kolicina()
    print "  3)Opis: "+komponenta.get_opis()
    print "  4)KATEGORIJA: "+ komponenta.get_kategorija().get_naziv()
    komanda = raw_input('Odaberite broj stavke koju zelite da promenite, ili pritisnite taster x za kraj: >>>')
    if komanda=='1':
        cena = raw_input('Unesite novu cenu za komponentu: >>>')
        komponenta.set_cena(cena)
        return komponenta
    elif komanda =='2':
        raspoloziva_kolicina = raw_input('Unesite novu raspolozivu kolicinu komponente: >>>')
        komponenta.get_raspoloziva_kolicina(raspoloziva_kolicina)
        return komponenta
    elif komanda =='3':
        opis = raw_input('Unesite novi opis komponente: >>>')
        komponenta.set_opis(opis)
        return komponenta
    elif komanda =='4':
        br = 0
        print 'KATEGORIJE: '
        for kat in lkategorije:
            print str(br)+")"+kat.get_naziv() #Integer br se mora pretvoriti u string kastovanjem 'str(br)' inace se ne moze ispisatei sa stringom, dakle oba moraju biti string!!!
            br += 1
        brkat = raw_input('Odabeite broj kategorije: >>>')  #Broj kategorije koju dodajemo komponenti
        if brkat == 'x':
            return None
        kat = lkategorije[int(brkat)]           # String pretvaramo natrag u integer jer nam treba broj indexa koji vadimo
        komponenta.set_kategorija(kat)
        return komponenta
    elif komanda =='x':
        return None
def promeniKategoriju(kategorija):
    os.system("clear")
    print 'IZABRANA KATEGORIJA: ' + kategorija.get_naziv()
    print '0)opis: ' + kategorija.get_opis()
    komanda = raw_input("Odaberite broj stavke koju zelite da promenite, ili pritisnite taster x za kraj: >>>")
    if komanda == '0':
        opis = raw_input('Unesite novi opis komponente: >>>')
        kategorija.set_opis(opis)
        return kategorija
    elif komanda =='x':
        return None
def promeniUredaj(uredaj):
    os.system("clear")
    print 'IZABRANI UREDJAJ: ' + uredaj.get_naziv()
    print '0)opis: ' + uredaj.get_opis()
    print '1)Komponente: ' + uredaj.displayKomponente()
    komanda = raw_input("Odaberite broj stavke koju zelite da promenite, ili pritisnite taster x za kraj: >>>")
    if komanda == '0':
        opis = raw_input('Unesite novi opis komponente: >>>')
        uredaj.set_opis(opis)
        return uredaj
    elif komanda =='x':
        return None
    
    
    
def sacuvajKomponente(thelist):
    thefile = open("komponente.txt", "w");
    data = None
    for item in thelist:
        data = item.get_naziv() + "|" +item.get_cena() + "|" + item.get_raspoloziva_kolicina() + "|" + item.get_opis() + ":" + item.get_kategorija().get_naziv() + "|" + item.get_kategorija().get_opis()
        thefile.write("%s\n" % data)
    return None

def sacuvajKategorije(thelist):
    thefile = open("kategorije.txt", "w");
    data = None
    for item in thelist:
        data = item.get_naziv() + "|" + item.get_opis()
        thefile.write("%s\n" % data)
    return None
    
def sacuvajUredaje(thelist):
    data = None
    thefile = open("uredaji.txt", "w");
    for item in thelist:
        data = item.get_naziv() + "|" + item.get_opis() + ":"
        thefile.write("%s\n" % item)
    return None