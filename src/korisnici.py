import os
from Korisnik import Korisnik

'''
Ovo sve moze i mnogo krace, 
korisnici se mogu direktno ucitavati u objekte
''' 
def str2Korisnik(strKorisnik):
    l=strKorisnik.split("|")
    recnikKorisnik={'korisnickoIme':l[0], 'lozinka':l[1], 'ime':l[2], 'prezime':l[3], 'uloga':l[4]}   
    return recnikKorisnik

def ucitajKorisnike():
    korisniciF=open('korisnici.txt','r')
    l=[]
    for red in korisniciF.readlines():
        l.append(str2Korisnik(red.strip()))    
    korisniciF.close()
    
    '''
        Do ove tacke smo ucitavali korisnike u listu l.
        Sada cemo da napravimo listu objekata korisnik i da dodajemo korisnike kao objekte.
    ''' 
    korisnici = []
    for i in l :
        korisnik = Korisnik(i['korisnickoIme'], i['lozinka'], i['ime'], i['prezime'], i['uloga'])
        korisnici.append(korisnik)
    '''
    Sada, umesto da vracamo dictionary (return l) vracamo listu objekata (return korisnici) ;)
    ''' 
    return korisnici

def jeRegistrovan(korisnickoIme, lozinka, korisnici):
    for korisnik in korisnici:
        if korisnickoIme==korisnik.get_korisnicko_ime() and lozinka==korisnik.get_lozinka():
            return korisnik
    return None      

def prijava(korisnici):
    while True:
        os.system("clear")            
        korisnickoIme=raw_input('korisnicko ime >>>')
        lozinka=raw_input('lozinka >>>')
        korisnik=jeRegistrovan(korisnickoIme,lozinka,korisnici)
        if korisnik!=None:
            return korisnik    

#if __name__=='__main__':
#    korisnici=ucitajKorisnike()
#    korisnik=prijava(korisnici)
#    print korisnik

