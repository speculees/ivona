from  Komponenta import Komponenta
from  Uredaj import Uredaj
from  Racun import Racun
from  Kategorija import Kategorija
import os

'''
Created on Jan 9, 2014

@author: Ivona
'''

def unesiNovuKomponentu():    
    os.system("clear")   
    print "NOVA KOMPONENTA: "         
    naziv =                 raw_input('naziv >>>')
    cena =                  raw_input('cena >>>')
    raspoloziva_kolicina =  raw_input('raspoloziva_kolicina >>>')
    opis =                  raw_input('opis >>>')
    komponenta = Komponenta(naziv, cena, raspoloziva_kolicina, opis)
    return komponenta

def unesiNovuKategoriju():
    os.system("clear")
    print "NOVA KATEGORIJA: "
    naziv = raw_input('naziv >>>')
    opis =  raw_input('opis >>>')
    kategorija = Kategorija(naziv, opis)
    return kategorija

def unesiNoviUredaj():
    os.system("clear")
    print "NOVI UREDJAJ: "
    naziv = raw_input('naziv >>>')
    opis =  raw_input('opis >>>')
    uredaj = Uredaj(naziv, opis)
    return uredaj

def unesiNoviRacun():
    os.system("clear")
    print "NOVI RACUN: "
    return True

def ucitajSacuvanuDatoteku():
    return True