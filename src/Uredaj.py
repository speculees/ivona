'''
Created on Jan 9, 2014

@author: Ivona
'''

class Uredaj:
    '''
    classdocs
    '''
    komponente = []


    def __init__(self, naziv, opis):
        '''
        Konstrukotor pravi svoju listu
        '''
        self.naziv=naziv
        self.opis=opis

    def get_naziv(self):
        return self.naziv


    def get_opis(self):
        return self.opis


    def set_naziv(self, value):
        self.naziv = value


    def set_opis(self, value):
        self.opis = value

        
        
        
    def addKomponenta(self, komponenta):
        Uredaj.komponente.append(komponenta)
        
    def removeKomponenta(self, value):
        Uredaj.komponente.remove(value)
        
    def toString(self):
        return str(self.naziv)+" "+str(self.opis)+" "+str(len(self.komponente)) #Pretvori u string    
    
    def displayKomponente(self):
        string=None
        for kom in Uredaj.komponente:
            string += str(kom.get_naziv()) + "\n"
        return string
    
    naziv = property(get_naziv, set_naziv, None, None)
    opis = property(get_opis, set_opis, None, None)
