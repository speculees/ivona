import korisnici
import unosi
import izmene
from  Korisnik import Korisnik
from  Komponenta import Komponenta
from  Uredaj import Uredaj
from  Racun import Racun
from  Kategorija import Kategorija
import os

def meniZaMenadzera():
    os.system("clear")    
    print "1)unos"
    print "2)brisanje"
    print "3)izmena"
    print "4)pretraga"
    print "5)izvestaji"
    print "6)odjava"
    print "x)izlaz"

def meniZaProdavca():
    os.system("clear")    
    print "1)unos"
    print "2)brisanje"
    print "3)izmena"
    print "4)pretraga"
    print "5)prodaja"
    print "6)sastavljanje uredjaja"
    print "7)odjava"
    print "x)izlaz"

def podmeniStavke():
    os.system("clear")    
    print "1)komponente"
    print "2)kategorije"
    print "3)uredjaji"
    print "x)nazad"

def podmeniPretragaPoKomponentama():
    os.system("clear")    
    print "1)naziv"
    print "2)opseg cena"
    print "3)raspoloziva kolicina"
    print "4)opis"
    print "5)kategorija"
    print "x)nazad"
        

def main():
    '''
    Prvo ucitavamo i kreiramo nove komponente, uredaje...
    '''
    lKorisnici=korisnici.ucitajKorisnike()              # Ovde pravimo listu korisnika, a ovo moze i krace ;)
    lkomponente =   []
    lkategorije =   []
    luredaji =      []
    
    komanda=""
    while komanda!="x":    
        print "Ucitani korisnici: " , lKorisnici
        prijavljeniKorisnik=korisnici.prijava(lKorisnici)   # Ovde korisnik vrsi prijavu!
        print prijavljeniKorisnik.get_ime()+" "+prijavljeniKorisnik.get_prezime()+" se uspesno prijavio na sistem kao "+prijavljeniKorisnik.get_uloga()+"!" 
        if prijavljeniKorisnik.get_uloga()=='prodavac':
            while True:            
                meniZaProdavca()
                komanda=raw_input(">>>")
                if komanda=='1':
                    podmeniStavke()
                    komanda=raw_input(">>>")
                    if komanda=='1':
                        '''
                        Ovde unosimo novu komponentu, ali
                        ne mozemo uneti komponentu ako ne postoji ni jedna kategorija, pa cemo proveriti
                        listu kategorija i ako je prazna dati upozorenje, a zatim pozvati 
                        metodu koja pravi kategoriju.
                        '''
                        if not lkategorije:
                            os.system('clear')
                            print  'lista kategorija je prazna! Prvo unesite bar jednu kategoriju...'
                            kategorija = unosi.unesiNovuKategoriju()
                            lkategorije.append(kategorija)
                            izmene.sacuvajKategorije(lkategorije)
                            print 'prva kategorija je uspesno dodata sada mozete uneti komponentu.'
                        while komanda != 'x':
                            br = 0
                            komponenta = unosi.unesiNovuKomponentu()
                            print 'Ponudjene su Vam kategorije: '
                            for kat in lkategorije:
                                print str(br)+")"+kat.get_naziv() #Integer br se mora pretvoriti u string kastovanjem 'str(br)' inace se ne moze ispisatei sa stringom, dakle oba moraju biti string!!!
                                br += 1
                            brkat = raw_input('Izaberite kategoriju kojoj komponenta pripada >>>')  #Broj kategorije koju dodajemo komponenti
                            if brkat == 'x':
                                pass
                            kat = lkategorije[int(brkat)]        # String pretvaramo natrag u integer jer nam treba broj indexa koji vadimo
                            komponenta.set_kategorija(kat)
                            lkomponente.append(komponenta)
                            print 'uspesno ste uneli komponentu '+komponenta.get_naziv()
                            izmene.sacuvajKomponente(lkomponente)
                            komanda = raw_input('pritisnite bilo koji taster za dalji unos, ili taster x za kraj: >>>')
                        pass
                    if komanda=='2':
                        '''
                        Ovde unosimo novu kategoriju
                        '''
                        while komanda != 'x':
                            kategorija = unosi.unesiNovuKategoriju()
                            lkategorije.append(kategorija)
                            izmene.sacuvajKategorije(lkategorije)
                            komanda = raw_input('pritisnite bilo koji taster za dalji unos, ili taster x za kraj: >>>')
                        pass
                    if komanda=='3':
                        '''
                        Ovde unosimo novi uredjaj
                        '''
                        while komanda != 'x':
                            uredaj = unosi.unesiNoviUredaj()
                            while komanda != 'x':
                                br = 0
                                print 'Ponudjene su Vam komponente: '
                                for kom in lkomponente:
                                    print str(br)+")"+kom.get_naziv()                            
                                    br += 1
                                brkom = raw_input('Izaberite broj komponente: >>>')  #Broj komponente koju dodajemo uredjaju
                                kom = lkomponente[int(brkom)]
                                uredaj.addKomponenta(kom)
                                komanda = raw_input('pritisnite bilo koji taster za dalji unos komponenti, ili taster x za kraj: >>>')
                            luredaji.append(uredaj)
                            komanda = raw_input('pritisnite bilo koji taster za dalji unos uredjaja, ili taster x za kraj: >>>')
                        pass
                    if komanda=='x':
                        pass                    
                elif komanda=='2':
                    podmeniStavke()
                    komanda=raw_input(">>>")
                    if komanda=='1':
                        '''
                        Ovde brisemo komponentu
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'KOMPONENTE:'
                            for kom in lkomponente:
                                print str(br)+")"+kom.get_naziv()                            
                                br += 1
                            brkom = raw_input('odaberite komponentu koju zelite da uklonite: >>>')  #Broj komponente koju brisemo
                            lkomponente.pop(int(brkom))
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da nastavirte brisanje komponenti, ili taster x za kraj: >>>')
                        pass
                    if komanda=='2':
                        '''
                        Ovde  brisemo kategoriju
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'KATEGORIJE:'
                            for kat in lkategorije:
                                print str(br)+")"+kat.get_naziv()                            
                                br += 1
                            brkat = raw_input('odaberite kategoriju koju zelite da uklonite: >>>')  #Broj kategorije koju brisemo
                            lkategorije.pop(int(brkat))
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da nastavirte brisanje kategorija, ili taster x za kraj: >>>')
                        pass
                    if komanda=='3':
                        '''
                        Ovde brisemo uredjaj
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'UREDJAJI:'
                            for ure in luredaji:
                                print str(br)+")"+ure.get_naziv()                            
                                br += 1
                            brure = raw_input('odaberite uredjaj koji zelite da uklonite: >>>')  #Broj kategorije koju brisemo
                            luredaji.pop(int(brure))
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da nastavirte brisanje uredjaja, ili taster x za kraj: >>>')
                        pass
                    if komanda=='x':
                        pass                    
                elif komanda=='3':
                    podmeniStavke()
                    komanda=raw_input(">>>")
                    if komanda=='1':
                        '''
                        Ovde vrsimo izmene nad komponentom
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'KOMPONENTE:'
                            for kom in lkomponente:
                                print str(br)+")"+kom.get_naziv()                            
                                br += 1
                            brkom= raw_input('odaberite komponentu koju zelite da izmenite: >>>')  #Broj kategorije koju brisemo
                            kom = lkomponente[int(brkom)]
                            nova_komponenta = izmene.promeniKomponentu(kom, lkategorije)
                            if nova_komponenta == None:
                                print 'izmena otkazana...'
                            else:
                                lkomponente[int(brkom)]= nova_komponenta
                                nova_komponenta.displayKomponenta()
                                
                                #SINHRONIZACIJA!!! komponenti u uredjajima
                                
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da izmenite neku drugu komponentu, ili pritisnite taster x za kraj: >>>')
                        pass
                    if komanda=='2':
                        '''
                        Ovde vrsimo izmene nad kategorijom
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'KATEGORIJE:'
                            for kat in lkategorije:
                                print str(br)+")"+kat.get_naziv()                            
                                br += 1
                            brkat= raw_input('odaberite kategoriju koju zelite da izmenite: >>>')  #Broj kategorije koju brisemo
                            kat = lkategorije[int(brkat)]
                            nova_kategorija = izmene.promeniKategoriju(kat)
                            if nova_kategorija == None:
                                print 'izmena otkazana...'
                            else:
                                lkategorije[int(brkat)]= nova_kategorija
                                nova_kategorija.displayKategorija()
                                
                                #SINHRONIZACIJA!!! kategorija u komponentama
                                
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da izmenite neku drugu kategoriju, ili pritisnite taster x za kraj: >>>')
                        pass
                    if komanda=='3':
                        '''
                        Ovde vrsimo izmene nad uredjajem
                        '''
                        while komanda != 'x':
                            br = 0
                            print 'UREDJAJI:'
                            for ure in luredaji:
                                print str(br)+")"+ure.get_naziv()                            
                                br += 1
                            brure= raw_input('odaberite uredjaj koji zelite da izmenite: >>>')  #Broj kategorije koju brisemo
                            ure = luredaji[int(brure)]
                            novi_uredaj = izmene.promeniUredaj(ure)
                            if novi_uredaj == None:
                                print 'izmena otkazana...'
                            else:
                                luredaji[int(brure)]= novi_uredaj
                                novi_uredaj.toString()
                                
                                #SINHRONIZACIJA!!! kategorija u komponentama
                                
                            komanda = raw_input('pritisnite bilo koji taster ukoliko zelite da izmenite neki drugi uredjaj, ili pritisnite taster x za kraj: >>>')
                        pass
                    if komanda=='x':
                        pass                    
                elif komanda=='4':
                    podmeniStavke()
                    komanda=raw_input(">>>")
                    if komanda=='1':
                        podmeniPretragaPoKomponentama()
                        komanda=raw_input(">>>")
                        if komanda=='1':
                            pass
                        if komanda=='2':
                            pass
                        if komanda=='3':
                            pass
                        if komanda=='4':
                            pass
                        if komanda=='5':
                            pass
                        if komanda=='x':
                            pass  
                    if komanda=='2':
                        pass
                    if komanda=='3':
                        pass
                    if komanda=='x':
                        pass                    
                elif komanda=='5':
                    pass
                elif komanda=='6':
                    pass
                elif komanda=='7':
                    break
                elif komanda=='x':
                    print "Dovidjenja"
                    break
        elif prijavljeniKorisnik['uloga']=='menadzer':
            while True:            
                meniZaMenadzera()
                komanda=raw_input(">>>")
                if komanda=='1':
                    pass
                elif komanda=='2':
                    pass
                elif komanda=='3':
                    pass
                elif komanda=='4':
                    pass
                elif komanda=='5':
                    pass
                elif komanda=='6':
                    break
                elif komanda=='x':
                    print "Dovidjenja"
                    break
        else:
            print "nije prepoznata uloga"

if __name__=="__main__":
    '''
    korisnik1 = Korisnik("prvi","prvi","prvi","prvi","prvi") 
    
    kategorija1 = Kategorija("naziv", "opis opis opis")
    
    komponenta1 = Komponenta("naziv", "cena", 55, "opis opis", kategorija1)
    
    uredaj1 = Uredaj("naziv", "opis")
    
    uredaj1.addKomponenta(komponenta1)
    
    print uredaj1.toString()
    '''
    main()
