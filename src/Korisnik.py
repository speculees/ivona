'''
Created on Jan 9, 2014

@author: Ivona
'''

class Korisnik:
    '''
    @attention: Posto ovo moramo da proveravamo korisnika lakse i pravilnije je ako imam objekat korisnik
    '''
    def __init__(self, korisnicko_ime, lozinka, ime, prezime, uloga):
        '''
        konstruktor postavlja vrednosti sledecim atributima
        '''
        self.korisnicko_ime = korisnicko_ime
        self.lozinka = lozinka
        self.ime = ime
        self.prezime = prezime
        self.uloga=None
        if uloga=='menadzer':
            self.uloga = uloga
        elif uloga=='prodavac':
            self.uloga = uloga
        if self.uloga==None:
            print "Korisnik " , self.ime, " nema odgovarajucu ulogu i bice uklonjen!!!"

    def get_korisnicko_ime(self):
        return self.korisnicko_ime


    def get_lozinka(self):
        return self.lozinka


    def get_ime(self):
        return self.ime


    def get_prezime(self):
        return self.prezime


    def get_uloga(self):
        return self.uloga

    
    def displayEmployee(self):
        print "Ulogovani korisnik je: Ime : ", self.ime,  ", Prezime: ", self.prezime, ", Uloga: ", self.uloga
        
        
    korisnicko_ime = property(get_korisnicko_ime, None, None, None)
    lozinka = property(get_lozinka, None, None, None)
    ime = property(get_ime, None, None, None)
    prezime = property(get_prezime, None, None, None)
    uloga = property(get_uloga, None, None, None)
        
    
