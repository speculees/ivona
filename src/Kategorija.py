'''
Created on Jan 9, 2014

@author: Ivona
'''

class Kategorija:
    '''
    classdocs
    '''
    def __init__(self, naziv, opis):
        '''
        Constructor
        '''
        self.naziv=naziv
        self.opis=opis

    def get_naziv(self):
        return self.naziv


    def get_opis(self):
        return self.opis


    def set_opis(self, value):
        self.opis = value
        
    def displayKategorija(self):
        print 'Kategorija: ' + self.naziv + ', opis: ' + self.opis

        
    naziv = property(get_naziv, None, None, None)
    opis = property(get_opis, set_opis, None, None)
        
        