'''
Created on Jan 9, 2014

@author: Ivona
'''

class Komponenta:
    '''
    
    '''
    kategorija = None
    
    def __init__(self, naziv, cena, raspoloziva_kolicina, opis):
        '''
        Constructor
        '''
        self.naziv=naziv
        self.cena=cena
        self.raspoloziva_kolicina=raspoloziva_kolicina
        self.opis=opis
    
    def get_kategorija(self):
        return self.kategorija


    def set_kategorija(self, value):
        self.kategorija = value
        

    def get_naziv(self):
        return self.naziv


    def get_cena(self):
        return self.cena


    def get_raspoloziva_kolicina(self):
        return self.raspoloziva_kolicina


    def get_opis(self):
        return self.opis


    def set_cena(self, value):
        self.cena = value


    def set_raspoloziva_kolicina(self, value):
        self.raspoloziva_kolicina = value


    def set_opis(self, value):
        self.opis = value

    def displayKomponenta(self):
        print 'Komponenta: ' + self.naziv + ', cena: ' + self.cena + ', kolicina:' + self.raspoloziva_kolicina + ', pod kategorijom:' + self.kategorija.get_naziv()
        return None
        
    naziv = property(get_naziv, None, None, None)
    cena = property(get_cena, set_cena, None, None)
    raspoloziva_kolicina = property(get_raspoloziva_kolicina, set_raspoloziva_kolicina, None, None)
    opis = property(get_opis, set_opis, None, None)
        
    