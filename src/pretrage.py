'''
Created on Jan 9, 2014

@author: speculees
'''

def nadjiKategorijuPoNazivu(lkategorije, naziv):
    for kategorija in lkategorije:
        if kategorija.get_naziv() == naziv:
            return kategorija
        else:
            return None
    
def nadjiKategorijuPoOpisu(lkategorije, opis):
    for kategorija in lkategorije:
        if kategorija.get_opis() == opis:
            return kategorija
        else:
            return None